﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace YouTubeProcessor
{
    public class RESTRequest
    {
        public static async Task<dynamic> GetJSON(string url)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    response.Content.Headers.ContentType.MediaType = "application/json";
                    return response.Content.ReadAsAsync<ExpandoObject>().Result;
                }
                else
                    throw new HttpRequestException("Error in GetJSON() with param: " + url);
            }
        }

        public static async Task<string> GetHTML(string url)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html"));

                HttpResponseMessage response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsStringAsync().Result; 
                }
                else
                    throw new HttpRequestException("Error in GetHTML() with param: " + url);
            }
        }

        public static async Task<dynamic> GetXML(string url)
        {
            var request = WebRequest.Create(url) as HttpWebRequest;
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            var xDoc = XDocument.Load((await request.GetResponseAsync()).GetResponseStream());

            return ConvertXMLToDynamic(xDoc.Elements().First());
        }

        public static async Task<dynamic> PostJSON(string url, Dictionary<string, string> body)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.PostAsync(url, new FormUrlEncodedContent(body));
                if (response.IsSuccessStatusCode)
                {
                    response.Content.Headers.ContentType.MediaType = "application/json";
                    return response.Content.ReadAsAsync<ExpandoObject>().Result;
                }
                else
                    throw new HttpRequestException("Error in PostJSON() with params: " + url + " body: " + body.ToString());
            }
        }

        static string GetbaseAddress(string url)
        {
            return url.Substring(0, url.IndexOf("?"));
        }

        static string GetParams(string url)
        {
            return url.Substring(url.IndexOf("?"));
        }

        static dynamic ConvertXMLToDynamic(XElement parent)
        {
            dynamic output = new ExpandoObject();

            output.Name = parent.Name.LocalName;
            output.Value = parent.Value;

            output.HasAttributes = parent.HasAttributes;
            if (parent.HasAttributes)
            {
                output.Attributes = new List<KeyValuePair<string, string>>();
                foreach (XAttribute attr in parent.Attributes())
                {
                    KeyValuePair<string, string> temp = new KeyValuePair<string, string>(attr.Name.LocalName, attr.Value);
                    output.Attributes.Add(temp);
                }
            }

            output.HasElements = parent.HasElements;
            if (parent.HasElements)
            {
                output.Elements = new List<dynamic>();
                foreach (XElement element in parent.Elements())
                {
                    dynamic temp = ConvertXMLToDynamic(element);
                    output.Elements.Add(temp);
                }
            }

            return output;
        }
    }
}
