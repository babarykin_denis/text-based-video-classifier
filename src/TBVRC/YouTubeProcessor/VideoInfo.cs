﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeProcessor
{
    public class VideoInfo : ICloneable
    {
        public VideoInfo()
        {
            Tags = new List<string>();
            Comments = new List<string>();
            SubtitlesExist = false;
        }

        public string Title { get; set; }

        public string Description { get; set; }

        public YouTubeGenreName Category { get; set; }

        public List<string> Tags { get; set; }

        public List<string> Comments { get; set; }

        public string Subtitles { get; set; }

        public bool TagsExist
        {
            get { return (Tags != null && Tags.Count > 0) ? true : false; }
        }

        public bool SubtitlesExist { get; set; }

        public string ThumbnailUrl { get; set; }

        public object Clone()
        {
            return new VideoInfo()
            {
                Title = (this.Title != null) ? this.Title.Clone().ToString() : null,
                Description = (this.Description != null) ? this.Description.Clone().ToString() : null,
                Category = this.Category,
                Tags = (this.Tags != null) ? this.Tags.Select(x => x.Clone().ToString()).ToList() : null,
                SubtitlesExist = this.SubtitlesExist,
                Comments = (this.Comments != null) ? this.Comments.Select(x => x.Clone().ToString()).ToList() : null,
                Subtitles = (this.Subtitles != null) ? this.Subtitles.Clone().ToString() : null,
                ThumbnailUrl = (this.ThumbnailUrl != null) ? this.ThumbnailUrl.Clone().ToString() : null
            };
        }
    }
}
