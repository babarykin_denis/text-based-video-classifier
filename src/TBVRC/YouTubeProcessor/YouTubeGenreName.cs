﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeProcessor
{
    /// <summary>
    /// YouTube categories list with ids.
    /// </summary>
    public enum YouTubeGenreName
    {
        FilmAndAnimation = 1,
        AutosAndVehicles = 2,
        Music = 10,
        PetsAndAnimals = 15,
        Sports = 17,
        ShortMovies = 18,
        TravelAndEvents = 19,
        Gaming = 20,
        Videoblogging = 21,
        PeopleAndBlogs = 22,
        Comedy = 23,
        Entertainment = 24,
        NewsAndPolitics = 25,
        HowtoAndStyle = 26,
        Education = 27,
        ScienceAndTechnology = 28,
        NonprofitsAndActivism = 29,
        Movies = 30,
        AnimeOrAnimation = 31,
        ActionOrAdventure = 32,
        Classics = 33,
        //Comedy = 34,
        Documentary = 35,
        Drama = 36,
        Family = 37,
        Foreign = 38,
        Horror = 39,
        SciFiOrFantasy = 40,
        Thriller = 41,
        Shorts = 42,
        Shows = 43,
        Trailers = 44
    }
}
