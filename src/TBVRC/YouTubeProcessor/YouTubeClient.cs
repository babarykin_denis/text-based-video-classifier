﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeProcessor
{
    public class YouTubeClient
    {
        /// <param name="urlOrId">Full Url or video id on youtube.</param>
        public YouTubeClient(string urlOrId)
        {
            if (urlOrId == null)
                throw new ArgumentNullException("Nullable video url was passed.");
            if (urlOrId == "")
                throw new ArgumentException("Empty video url was passed.");

            if (urlOrId.Contains("youtube.com"))
                VideoId = GetVideoIdFromUrl(urlOrId);
            else
                VideoId = urlOrId;
        }

        string VideoId { get; set; }

        public async Task<VideoInfo> GetVideoInfo(bool useSubs = true)
        {
            dynamic mainResponse = await RESTRequest.GetJSON("https://www.googleapis.com/youtube/v3/videos?part=snippet&key=AIzaSyCgqL0_iYq3d38fD7ERaYRCTqAXBCBCGe4&id=" + VideoId);
            var title = mainResponse.items[0].snippet.title;
            var description = mainResponse.items[0].snippet.description;
            var category = (YouTubeGenreName)int.Parse(mainResponse.items[0].snippet.categoryId);
            var thumbnailUrl = mainResponse.items[0].snippet.thumbnails.@default.url;
            List<string> comments = new List<string>();
            try
            {
                dynamic commentsResponse = await RESTRequest.GetJSON("https://www.googleapis.com/youtube/v3/commentThreads?key=AIzaSyCgqL0_iYq3d38fD7ERaYRCTqAXBCBCGe4&textFormat=plainText&part=snippet&maxResults=100&videoId=" + VideoId);
                comments = GetCommentsFromResponse(commentsResponse);
            }
            catch
            {
                comments = new List<string>();
            }

            List<string> tags;
            try
            {
                List<object> tagsObjects = mainResponse.items[0].snippet.tags;
                tags = tagsObjects.Select(o => o.ToString()).ToList();
            }
            catch(Exception e)
            {
                tags = new List<string>();
            }

            string subtitles = null, subtitlesUrl = null;
            try
            {
                subtitlesUrl = await GetSubtitlesUrl();
                if (useSubs && subtitlesUrl != null)
                    subtitles = (await RESTRequest.GetXML(subtitlesUrl)).Value;
            }
            catch(Exception e)
            {
                subtitles = null;
            }

            VideoInfo videoInfo = new VideoInfo
            {
                Title = title,
                Description = description,
                Tags = tags,
                Category = category,
                Comments = comments,
                Subtitles = subtitles,
                SubtitlesExist = (subtitlesUrl != null) ? true : false,
                ThumbnailUrl = thumbnailUrl
            };

            return videoInfo;
        }

        public async Task<VideoInfo> TranslateVideoInfo(VideoInfo videoInfo)
        {
            VideoInfo translatedVideoInfo = (VideoInfo)videoInfo.Clone();

            translatedVideoInfo.Title = await GetTranslation(videoInfo.Title);
            translatedVideoInfo.Description = await GetTranslation(videoInfo.Description);

            if (videoInfo.TagsExist)
            {
                translatedVideoInfo.Tags.Clear();
                foreach (string tag in videoInfo.Tags)
                    translatedVideoInfo.Tags.Add(await GetTranslation(tag));
            }

            translatedVideoInfo.Comments.Clear();
            foreach (string comment in videoInfo.Comments)
                translatedVideoInfo.Comments.Add(await GetTranslation(comment));

            if (videoInfo.Subtitles != null && videoInfo.Subtitles.Length > 0)
                translatedVideoInfo.Subtitles = await GetTranslation(videoInfo.Subtitles); // TODO try catch to subs!

            return translatedVideoInfo;
        }

        string GetVideoIdFromUrl(string url)
        {
            string id = url.Substring(url.LastIndexOf("?v=") + 3);
            if (id.Contains('&'))
            {
                int idLen = id.IndexOf('&');
                id = id.Substring(0, idLen);
            }

            return id;
        }

        List<string> GetCommentsFromResponse(dynamic response)
        {
            var comments = new List<string>();
            foreach (var item in response.items)
                comments.Add(item.snippet.topLevelComment.snippet.textDisplay);
            return comments;
        }

        async Task<string> GetSubtitlesUrl()
        {
            string html = await RESTRequest.GetHTML("http://downsub.com/?url=+https://www.youtube.com/watch?v=" + VideoId);
            int startShiftInd = html.IndexOf("<b><a href=\"./index.php?title=");
            if (startShiftInd >= 0)
            {
                html = html.Substring(startShiftInd);
                int startInd = html.IndexOf("https%3A%2F%2Fwww.youtube.com");
                int endInd = html.IndexOf("\">>>Download<<</a>");
                string url = html.Substring(startInd, endInd - startInd);

                url = url.Replace("%3A", ":").Replace("%2F", "/").Replace("%3F", "?").Replace("%3D", "=").Replace("%26", "&").Replace("%25", "%");

                return url + "&tlang=en";
            }
            else
                return null;

        }

        async Task<string> GetTranslation(string stringToTranslate)
        {
            const int MAX_STRING_LEN = 1000;
            string res = "";
            int parts = stringToTranslate.Length / MAX_STRING_LEN + 1;
            for (int i = 0; i < parts; ++i)
            {
                int count = (stringToTranslate.Length - i * MAX_STRING_LEN < MAX_STRING_LEN) ? (stringToTranslate.Length - i * MAX_STRING_LEN) : MAX_STRING_LEN;
                string currentPartString = stringToTranslate.Substring(i * MAX_STRING_LEN, count); 
                dynamic resp = await RESTRequest.PostJSON("https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20160522T163920Z.8ec2e4a27f1f13b2.fd2485ee2333d4ce836bab4fb01b5d13f2e1a309&lang=en", new Dictionary<string, string>() { { "text", currentPartString } });
                res += resp.text[0];
            }
            return res;
        }
    }
}
