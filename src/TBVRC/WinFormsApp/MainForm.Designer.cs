﻿namespace WinFormsApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtbxUrl = new System.Windows.Forms.TextBox();
            this.lblUrl = new System.Windows.Forms.Label();
            this.lblYTCategory = new System.Windows.Forms.Label();
            this.btnPredict = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblGenre = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.pctrbxThumbnail = new System.Windows.Forms.PictureBox();
            this.lblSubs = new System.Windows.Forms.Label();
            this.grpbxOptions = new System.Windows.Forms.GroupBox();
            this.chckbxSubs = new System.Windows.Forms.CheckBox();
            this.chckbxTranslate = new System.Windows.Forms.CheckBox();
            this.grpbxInfo = new System.Windows.Forms.GroupBox();
            this.grpbxThumbnail = new System.Windows.Forms.GroupBox();
            this.grpbxCharts = new System.Windows.Forms.GroupBox();
            this.winChartViewer = new ChartDirector.WinChartViewer();
            this.grpbxResults = new System.Windows.Forms.GroupBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.rbtnDescriptor = new System.Windows.Forms.RadioButton();
            this.rbtnLists = new System.Windows.Forms.RadioButton();
            this.rbtnAbsolete = new System.Windows.Forms.RadioButton();
            this.rbtnRelative = new System.Windows.Forms.RadioButton();
            this.grpbxDisType = new System.Windows.Forms.GroupBox();
            this.grpbxVideResType = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctrbxThumbnail)).BeginInit();
            this.grpbxOptions.SuspendLayout();
            this.grpbxInfo.SuspendLayout();
            this.grpbxThumbnail.SuspendLayout();
            this.grpbxCharts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.winChartViewer)).BeginInit();
            this.grpbxResults.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.grpbxDisType.SuspendLayout();
            this.grpbxVideResType.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtbxUrl
            // 
            this.txtbxUrl.Location = new System.Drawing.Point(12, 41);
            this.txtbxUrl.Name = "txtbxUrl";
            this.txtbxUrl.Size = new System.Drawing.Size(326, 22);
            this.txtbxUrl.TabIndex = 0;
            // 
            // lblUrl
            // 
            this.lblUrl.AutoSize = true;
            this.lblUrl.Location = new System.Drawing.Point(9, 22);
            this.lblUrl.Name = "lblUrl";
            this.lblUrl.Size = new System.Drawing.Size(102, 16);
            this.lblUrl.TabIndex = 1;
            this.lblUrl.Text = "Вставьте URL:";
            // 
            // lblYTCategory
            // 
            this.lblYTCategory.AutoSize = true;
            this.lblYTCategory.Location = new System.Drawing.Point(9, 113);
            this.lblYTCategory.MaximumSize = new System.Drawing.Size(500, 16);
            this.lblYTCategory.Name = "lblYTCategory";
            this.lblYTCategory.Size = new System.Drawing.Size(137, 16);
            this.lblYTCategory.TabIndex = 2;
            this.lblYTCategory.Text = "YouTube категория:";
            // 
            // btnPredict
            // 
            this.btnPredict.Location = new System.Drawing.Point(344, 40);
            this.btnPredict.Name = "btnPredict";
            this.btnPredict.Size = new System.Drawing.Size(99, 24);
            this.btnPredict.TabIndex = 3;
            this.btnPredict.Text = "Рассчитать";
            this.btnPredict.UseVisualStyleBackColor = true;
            this.btnPredict.Click += new System.EventHandler(this.butnPredict_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(449, 40);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(78, 24);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Очистить";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblGenre
            // 
            this.lblGenre.AutoSize = true;
            this.lblGenre.Location = new System.Drawing.Point(6, 23);
            this.lblGenre.Name = "lblGenre";
            this.lblGenre.Size = new System.Drawing.Size(142, 16);
            this.lblGenre.TabIndex = 6;
            this.lblGenre.Text = "Рассчитанный жанр:";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(9, 68);
            this.lblTitle.MaximumSize = new System.Drawing.Size(500, 16);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(120, 16);
            this.lblTitle.TabIndex = 7;
            this.lblTitle.Text = "Название видео:";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(9, 90);
            this.lblDescription.MaximumSize = new System.Drawing.Size(500, 16);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(119, 16);
            this.lblDescription.TabIndex = 8;
            this.lblDescription.Text = "Описание видео:";
            // 
            // pctrbxThumbnail
            // 
            this.pctrbxThumbnail.Location = new System.Drawing.Point(6, 16);
            this.pctrbxThumbnail.Name = "pctrbxThumbnail";
            this.pctrbxThumbnail.Size = new System.Drawing.Size(120, 90);
            this.pctrbxThumbnail.TabIndex = 9;
            this.pctrbxThumbnail.TabStop = false;
            // 
            // lblSubs
            // 
            this.lblSubs.AutoSize = true;
            this.lblSubs.Location = new System.Drawing.Point(9, 134);
            this.lblSubs.MaximumSize = new System.Drawing.Size(500, 16);
            this.lblSubs.Name = "lblSubs";
            this.lblSubs.Size = new System.Drawing.Size(75, 16);
            this.lblSubs.TabIndex = 10;
            this.lblSubs.Text = "Субтитры:";
            // 
            // grpbxOptions
            // 
            this.grpbxOptions.Controls.Add(this.chckbxSubs);
            this.grpbxOptions.Controls.Add(this.chckbxTranslate);
            this.grpbxOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpbxOptions.Location = new System.Drawing.Point(552, 0);
            this.grpbxOptions.Name = "grpbxOptions";
            this.grpbxOptions.Size = new System.Drawing.Size(288, 272);
            this.grpbxOptions.TabIndex = 12;
            this.grpbxOptions.TabStop = false;
            this.grpbxOptions.Text = "Опции";
            // 
            // chckbxSubs
            // 
            this.chckbxSubs.AutoSize = true;
            this.chckbxSubs.Location = new System.Drawing.Point(17, 42);
            this.chckbxSubs.Name = "chckbxSubs";
            this.chckbxSubs.Size = new System.Drawing.Size(186, 20);
            this.chckbxSubs.TabIndex = 1;
            this.chckbxSubs.Text = "Использовать субтитры";
            this.chckbxSubs.UseVisualStyleBackColor = true;
            // 
            // chckbxTranslate
            // 
            this.chckbxTranslate.AutoSize = true;
            this.chckbxTranslate.Location = new System.Drawing.Point(17, 19);
            this.chckbxTranslate.Name = "chckbxTranslate";
            this.chckbxTranslate.Size = new System.Drawing.Size(150, 20);
            this.chckbxTranslate.TabIndex = 0;
            this.chckbxTranslate.Text = "Включить перевод";
            this.chckbxTranslate.UseVisualStyleBackColor = true;
            // 
            // grpbxInfo
            // 
            this.grpbxInfo.Controls.Add(this.grpbxThumbnail);
            this.grpbxInfo.Controls.Add(this.lblSubs);
            this.grpbxInfo.Controls.Add(this.btnClear);
            this.grpbxInfo.Controls.Add(this.txtbxUrl);
            this.grpbxInfo.Controls.Add(this.btnPredict);
            this.grpbxInfo.Controls.Add(this.lblUrl);
            this.grpbxInfo.Controls.Add(this.lblTitle);
            this.grpbxInfo.Controls.Add(this.lblDescription);
            this.grpbxInfo.Controls.Add(this.lblYTCategory);
            this.grpbxInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpbxInfo.Location = new System.Drawing.Point(12, 0);
            this.grpbxInfo.Name = "grpbxInfo";
            this.grpbxInfo.Size = new System.Drawing.Size(534, 272);
            this.grpbxInfo.TabIndex = 13;
            this.grpbxInfo.TabStop = false;
            this.grpbxInfo.Text = "Информация о видео";
            // 
            // grpbxThumbnail
            // 
            this.grpbxThumbnail.Controls.Add(this.pctrbxThumbnail);
            this.grpbxThumbnail.Location = new System.Drawing.Point(6, 153);
            this.grpbxThumbnail.Name = "grpbxThumbnail";
            this.grpbxThumbnail.Size = new System.Drawing.Size(132, 113);
            this.grpbxThumbnail.TabIndex = 11;
            this.grpbxThumbnail.TabStop = false;
            this.grpbxThumbnail.Text = "Превью";
            // 
            // grpbxCharts
            // 
            this.grpbxCharts.Controls.Add(this.grpbxVideResType);
            this.grpbxCharts.Controls.Add(this.grpbxDisType);
            this.grpbxCharts.Controls.Add(this.winChartViewer);
            this.grpbxCharts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpbxCharts.Location = new System.Drawing.Point(12, 272);
            this.grpbxCharts.Name = "grpbxCharts";
            this.grpbxCharts.Size = new System.Drawing.Size(1240, 385);
            this.grpbxCharts.TabIndex = 14;
            this.grpbxCharts.TabStop = false;
            this.grpbxCharts.Text = "Графики";
            // 
            // winChartViewer
            // 
            this.winChartViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.winChartViewer.Location = new System.Drawing.Point(211, 21);
            this.winChartViewer.Name = "winChartViewer";
            this.winChartViewer.Size = new System.Drawing.Size(1023, 358);
            this.winChartViewer.TabIndex = 0;
            this.winChartViewer.TabStop = false;
            this.winChartViewer.Paint += new System.Windows.Forms.PaintEventHandler(this.winChartViewer_Paint);
            // 
            // grpbxResults
            // 
            this.grpbxResults.Controls.Add(this.lblGenre);
            this.grpbxResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpbxResults.Location = new System.Drawing.Point(846, 0);
            this.grpbxResults.Name = "grpbxResults";
            this.grpbxResults.Size = new System.Drawing.Size(406, 272);
            this.grpbxResults.TabIndex = 15;
            this.grpbxResults.TabStop = false;
            this.grpbxResults.Text = "Результаты";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar,
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 659);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1264, 22);
            this.statusStrip.TabIndex = 16;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel.Text = "Готово";
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // rbtnDescriptor
            // 
            this.rbtnDescriptor.AutoSize = true;
            this.rbtnDescriptor.Checked = true;
            this.rbtnDescriptor.Location = new System.Drawing.Point(6, 21);
            this.rbtnDescriptor.Name = "rbtnDescriptor";
            this.rbtnDescriptor.Size = new System.Drawing.Size(160, 20);
            this.rbtnDescriptor.TabIndex = 1;
            this.rbtnDescriptor.TabStop = true;
            this.rbtnDescriptor.Text = "частям дескриптора";
            this.rbtnDescriptor.UseVisualStyleBackColor = true;
            this.rbtnDescriptor.CheckedChanged += new System.EventHandler(this.rbtnDescriptor_CheckedChanged);
            // 
            // rbtnLists
            // 
            this.rbtnLists.AutoSize = true;
            this.rbtnLists.Location = new System.Drawing.Point(6, 47);
            this.rbtnLists.Name = "rbtnLists";
            this.rbtnLists.Size = new System.Drawing.Size(193, 20);
            this.rbtnLists.TabIndex = 3;
            this.rbtnLists.Text = "спискам классификатора";
            this.rbtnLists.UseVisualStyleBackColor = true;
            // 
            // rbtnAbsolete
            // 
            this.rbtnAbsolete.AutoSize = true;
            this.rbtnAbsolete.Checked = true;
            this.rbtnAbsolete.Location = new System.Drawing.Point(6, 22);
            this.rbtnAbsolete.Name = "rbtnAbsolete";
            this.rbtnAbsolete.Size = new System.Drawing.Size(107, 20);
            this.rbtnAbsolete.TabIndex = 4;
            this.rbtnAbsolete.TabStop = true;
            this.rbtnAbsolete.Text = "Абсолютное";
            this.rbtnAbsolete.UseVisualStyleBackColor = true;
            this.rbtnAbsolete.CheckedChanged += new System.EventHandler(this.rbtnAbsolete_CheckedChanged);
            // 
            // rbtnRelative
            // 
            this.rbtnRelative.AutoSize = true;
            this.rbtnRelative.Location = new System.Drawing.Point(6, 48);
            this.rbtnRelative.Name = "rbtnRelative";
            this.rbtnRelative.Size = new System.Drawing.Size(162, 20);
            this.rbtnRelative.TabIndex = 5;
            this.rbtnRelative.Text = "Относительное (в %)";
            this.rbtnRelative.UseVisualStyleBackColor = true;
            // 
            // grpbxDisType
            // 
            this.grpbxDisType.Controls.Add(this.rbtnRelative);
            this.grpbxDisType.Controls.Add(this.rbtnAbsolete);
            this.grpbxDisType.Location = new System.Drawing.Point(6, 21);
            this.grpbxDisType.Name = "grpbxDisType";
            this.grpbxDisType.Size = new System.Drawing.Size(200, 74);
            this.grpbxDisType.TabIndex = 6;
            this.grpbxDisType.TabStop = false;
            this.grpbxDisType.Text = "Вид распределения:";
            // 
            // grpbxVideResType
            // 
            this.grpbxVideResType.Controls.Add(this.rbtnDescriptor);
            this.grpbxVideResType.Controls.Add(this.rbtnLists);
            this.grpbxVideResType.Location = new System.Drawing.Point(6, 101);
            this.grpbxVideResType.Name = "grpbxVideResType";
            this.grpbxVideResType.Size = new System.Drawing.Size(200, 74);
            this.grpbxVideResType.TabIndex = 7;
            this.grpbxVideResType.TabStop = false;
            this.grpbxVideResType.Text = "Распределение по:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.grpbxResults);
            this.Controls.Add(this.grpbxCharts);
            this.Controls.Add(this.grpbxInfo);
            this.Controls.Add(this.grpbxOptions);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Автоматизированный классификатор видео по текстовой информации";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pctrbxThumbnail)).EndInit();
            this.grpbxOptions.ResumeLayout(false);
            this.grpbxOptions.PerformLayout();
            this.grpbxInfo.ResumeLayout(false);
            this.grpbxInfo.PerformLayout();
            this.grpbxThumbnail.ResumeLayout(false);
            this.grpbxCharts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.winChartViewer)).EndInit();
            this.grpbxResults.ResumeLayout(false);
            this.grpbxResults.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.grpbxDisType.ResumeLayout(false);
            this.grpbxDisType.PerformLayout();
            this.grpbxVideResType.ResumeLayout(false);
            this.grpbxVideResType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtbxUrl;
        private System.Windows.Forms.Label lblUrl;
        private System.Windows.Forms.Label lblYTCategory;
        private System.Windows.Forms.Button btnPredict;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblGenre;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.PictureBox pctrbxThumbnail;
        private System.Windows.Forms.Label lblSubs;
        private System.Windows.Forms.GroupBox grpbxOptions;
        private System.Windows.Forms.CheckBox chckbxTranslate;
        private System.Windows.Forms.CheckBox chckbxSubs;
        private System.Windows.Forms.GroupBox grpbxInfo;
        private System.Windows.Forms.GroupBox grpbxCharts;
        private System.Windows.Forms.GroupBox grpbxResults;
        private System.Windows.Forms.GroupBox grpbxThumbnail;
        private ChartDirector.WinChartViewer winChartViewer;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.GroupBox grpbxVideResType;
        private System.Windows.Forms.RadioButton rbtnDescriptor;
        private System.Windows.Forms.RadioButton rbtnLists;
        private System.Windows.Forms.GroupBox grpbxDisType;
        private System.Windows.Forms.RadioButton rbtnRelative;
        private System.Windows.Forms.RadioButton rbtnAbsolete;
    }
}

