﻿using ChartDirector;
using Classifier;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YouTubeProcessor;

namespace WinFormsApp
{
    public partial class MainForm : Form
    {
        GenreClassifier Classifier { get; set; }

        List<GenreResult> GResults { get; set; }

        public MainForm()
        {
            InitializeComponent();
            Classifier = new GenreClassifier();
            GResults = null;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Clear();
            txtbxUrl.Text = "https://www.youtube.com/watch?v=F1jpCBPiWS4";
        }

        private async void butnPredict_Click(object sender, EventArgs e)
        {
            if (txtbxUrl.Text == "")
            {
                MessageBox.Show("Введите ссылку на видео!", "Ошибка входных данных");
                return;
            }
            var temp = txtbxUrl.Text.Clone().ToString();
            Clear();
            txtbxUrl.Text = temp;

            toolStripStatusLabel.Text = "Получение информации о видео";
            toolStripProgressBar.Visible = true;
            toolStripProgressBar.Value = 30;

            var youTubeClient = new YouTubeClient(txtbxUrl.Text);
            var videoInfo = await youTubeClient.GetVideoInfo(chckbxSubs.Checked);

            lblYTCategory.Text = "YouTube категория: " + videoInfo.Category.ToString();
            lblTitle.Text = "Название видео: " + videoInfo.Title;
            lblDescription.Text = "Описание видео: " + videoInfo.Description;
            lblSubs.Text = "Субтитры: " + (videoInfo.SubtitlesExist ? "присутствуют" : "отсутствуют");
            pctrbxThumbnail.LoadAsync(videoInfo.ThumbnailUrl);

            toolStripStatusLabel.Text = (chckbxTranslate.Checked) ? "Перевод дескриптора видео" : "Расчёт жанра";
            toolStripProgressBar.Value = 60;

            #region viewer

            GResults = Classifier.PredictGenresDiscret((chckbxTranslate.Checked) ? (await youTubeClient.TranslateVideoInfo(videoInfo)) : videoInfo, chckbxSubs.Checked);

            toolStripStatusLabel.Text = "Построение графиков";
            toolStripProgressBar.Value = 90;

            RedrawGraphic();

            toolStripProgressBar.Value = 100;
            toolStripStatusLabel.Text = "Готово";
            toolStripProgressBar.Visible = false;

            #endregion
        }

        void ShowStackedHistogram(WinChartViewer viewer, List<List<double>> dataSets, List<string> dataSetsNames, string title, string yAxisTitle)
        {
            XYChart c = new XYChart(1023, 360);

            c.setColor(Chart.TextColor, 0x333333);
            c.setPlotArea(70, 20, 800, 300, Chart.Transparent, -1, Chart.Transparent, 0xcccccc);
            LegendBox b = c.addLegend(1023 - 150, 20, true, "Arial", 12);
            b.setBackground(Chart.Transparent, Chart.Transparent);
            b.setKeyBorder(Chart.SameAsMainColor);

            c.xAxis().setColors(Chart.Transparent);
            c.yAxis().setColors(Chart.Transparent);
            c.xAxis().setLabelStyle("Arial", 12);
            c.yAxis().setLabelStyle("Arial", 12);

            BarLayer layer = c.addBarLayer2(Chart.Stack);

            int[] colors = new int[] { 0xaaccee, 0xbbdd88, 0xeeaa66, 0xff6666, 0x6666ff };

            for (int i = 0; i < dataSets.Count; ++i)
                layer.addDataSet(dataSets[i].ToArray(), colors[i], dataSetsNames[i]);

            layer.setBorderColor(Chart.Transparent);
            layer.setAggregateLabelStyle("Arial", 12);
            layer.setDataLabelStyle("Arial", 10).setAlignment(Chart.Center);
            layer.setLegendOrder(Chart.ReverseLegend);

            c.xAxis().setLabels(Classifier.Genres.Select(genre => genre.Name).ToArray());
            c.yAxis().setTickDensity(40);
            c.yAxis().setTitle(yAxisTitle, "Arial Bold", 14, 0x555555);
            c.addTitle(title);

            viewer.Chart = c;

            viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                "title='{dataSetName} по жанру {xLabel}: {value} '");
            viewer.Show();

            
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            GResults = null;
            txtbxUrl.Clear();
            lblYTCategory.Text = "YouTube категория: ";
            lblGenre.Text = "Рассчитанный жанр: ";
            //chart.Series.Clear();
            //chart.Visible = false;
            //lblTitle.Visible = false;
            lblTitle.Text = "Название видео: ";
            lblDescription.Text = "Описание видео: ";
            //lblDescription.Visible = false;
            pctrbxThumbnail.Image = null;
            lblSubs.Text = "Субтитры: ";
            //lblSubs.Visible = false;
            winChartViewer.Hide();
            toolStripProgressBar.Visible = false;
        }

        private void rbtnDescriptor_CheckedChanged(object sender, EventArgs e)
        {
            RedrawGraphic();
        }

        void RedrawGraphic()
        {
            if (GResults != null)
            {
                double sum = 0;
                GResults.ForEach(g => sum += g.Sum);

                if (rbtnDescriptor.Checked && rbtnAbsolete.Checked)
                {
                    List<List<double>> partsDataSets = new List<List<double>>()
                    {
                        GResults.Select(gr => gr.TitleScore).ToList(),
                        GResults.Select(gr => gr.TagsScore).ToList(),
                        GResults.Select(gr => gr.DescriptionScore).ToList(),
                        GResults.Select(gr => gr.CommentsScore).ToList(),
                        GResults.Select(gr => gr.SubtitilesScore).ToList()
                    };
                    List<string> partsDataSetsnames = new List<string>() { "Название", "Тэги", "Описание", "Коммент.", "Субтитры" };
                    ShowStackedHistogram(winChartViewer, partsDataSets, partsDataSetsnames, "Абсолютное распределение по частям дескриптора", "Очки жанра");
                }
                else if (rbtnLists.Checked && rbtnAbsolete.Checked)
                {
                    List<List<double>> listsDataSets = new List<List<double>>()
                    {
                        GResults.Select(gr => gr.RootListScore).ToList(),
                        GResults.Select(gr => gr.SeedListScore).ToList(),
                        GResults.Select(gr => gr.SynonymsListScore).ToList(),
                        GResults.Select(gr => gr.SemanticListScore).ToList(),
                    };
                    List<string> listsDataSetsnames = new List<string>() { "Корневой", "Иниц.", "Синоним.", "Семант." };
                    ShowStackedHistogram(winChartViewer, listsDataSets, listsDataSetsnames, "Абсолютное распределение по спискам классификатора", "Очки жанра");
                }
                else if (rbtnDescriptor.Checked && rbtnRelative.Checked)
                {
                    List<List<double>> partsDataSets = new List<List<double>>()
                    {
                        GResults.Select(gr => gr.GetDescriptorScoresPercentage(sum)[0]).ToList(),
                        GResults.Select(gr => gr.GetDescriptorScoresPercentage(sum)[1]).ToList(),
                        GResults.Select(gr => gr.GetDescriptorScoresPercentage(sum)[2]).ToList(),
                        GResults.Select(gr => gr.GetDescriptorScoresPercentage(sum)[3]).ToList(),
                        GResults.Select(gr => gr.GetDescriptorScoresPercentage(sum)[4]).ToList()
                    };
                    List<string> partsDataSetsnames = new List<string>() { "Название", "Тэги", "Описание", "Коммент.", "Субтитры" };
                    ShowStackedHistogram(winChartViewer, partsDataSets, partsDataSetsnames, "Относительное распределение по частям дескриптора", "Проценты");
                }
                else if (rbtnLists.Checked && rbtnRelative.Checked)
                {
                    List<List<double>> listsDataSets = new List<List<double>>()
                    {
                        GResults.Select(gr => gr.GetListsScoresPercentage(sum)[0]).ToList(),
                        GResults.Select(gr => gr.GetListsScoresPercentage(sum)[1]).ToList(),
                        GResults.Select(gr => gr.GetListsScoresPercentage(sum)[2]).ToList(),
                        GResults.Select(gr => gr.GetListsScoresPercentage(sum)[3]).ToList(),
                    };
                    List<string> listsDataSetsnames = new List<string>() { "Корневой", "Иниц.", "Синоним.", "Семант." };
                    ShowStackedHistogram(winChartViewer, listsDataSets, listsDataSetsnames, "Относительное распределение по спискам классификатора", "Проценты");
                }
            }
        }

        private void winChartViewer_Paint(object sender, PaintEventArgs e)
        {
            Rectangle ee = new Rectangle(0, winChartViewer.Height - 12, winChartViewer.Width, 12);
            using (SolidBrush brush = new SolidBrush(Color.White))
            {
                e.Graphics.FillRectangle(brush, ee);
            }
        }

        private void rbtnAbsolete_CheckedChanged(object sender, EventArgs e)
        {
            RedrawGraphic();
        }
    }
}
