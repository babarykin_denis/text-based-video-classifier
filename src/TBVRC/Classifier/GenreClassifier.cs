﻿using LemmaSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTubeProcessor;

namespace Classifier
{
    public class GenreClassifier
    {
        public GenreClassifier()
        {
            Genres = new GenresFactory().CreateGenres();
        }

        List<string> StopWords { get; set; }

        const double w1 = 1, w2 = 0.67, w3 = 0.1, w4 = 0.001;           // weights for root words, seed list, synonyms list and semantic
        const double p1 = 1, p2 = 0.75, p3 = 0.5, p4 = 0.25, p5 = 0.25;   // weights for title, tags, description, comments and subtiles

        public List<Genre> Genres { get; set; }

        bool UseSubs { get; set; }

        public List<int> PredictGenres(VideoInfo videoInfo, bool useSubs = true)
        {
            UseSubs = useSubs;
            var videoDescriptor = new VideoDescriptor(videoInfo);
            var genrePoints = new int[Genres.Count];

            for (int i = 0; i < Genres.Count; ++i) // "for" cycle instead of "foreach" for disable dependency of compiler-optimization on result
            {
                var curGenre = Genres[i];
                double score = 0;

                //foreach (string word in curGenre.RootWords)
                //    score += w1 * CalcInsertionsOfWordToDescriptor(word, videoDescriptor);
                //foreach (string word in curGenre.SeedList)
                //    score += w2 * CalcInsertionsOfWordToDescriptor(word, videoDescriptor);
                //foreach (string word in curGenre.ConceptList)
                //    score += w3 * CalcInsertionsOfWordToDescriptor(word, videoDescriptor);

                genrePoints[i] = (int) Math.Round(score);
            }

            return genrePoints.ToList();
            //return new List<int>() { 100, 20, 40, 50, 30 };
        }

        public List<GenreResult> PredictGenresDiscret(VideoInfo videoInfo, bool useSubs = true)
        {
            UseSubs = useSubs;
            var videoDescriptor = new VideoDescriptor(videoInfo);
            List<GenreResult> results = new List<GenreResult>();

            for (int i = 0; i < Genres.Count; ++i) // "for" cycle instead of "foreach" for disable dependency of compiler-optimization on result
            {
                var curGenre = Genres[i];
                GenreResult genreResult = new GenreResult();

                foreach (string word in curGenre.RootWords)
                    genreResult.RootListScore += CalcInsertionsOfWordToDescriptor(word, videoDescriptor, genreResult, w1);
                foreach (string word in curGenre.SeedList)
                    genreResult.SeedListScore += CalcInsertionsOfWordToDescriptor(word, videoDescriptor, genreResult, w2);
                foreach (string word in curGenre.SynonymsList)
                    genreResult.SynonymsListScore += CalcInsertionsOfWordToDescriptor(word, videoDescriptor, genreResult, w3);
                foreach (string word in curGenre.SemanticList)
                    genreResult.SemanticListScore += CalcInsertionsOfWordToDescriptor(word, videoDescriptor, genreResult, w4);

                results.Add(genreResult);
            }

            return results;
        }

        public string DetermineGenre(List<int> genreScores)
        {
            int max = genreScores.Max();
            int index = 0;
            for (int i = 0; i < genreScores.Count; ++i)
                if (genreScores[i] == max)
                    index = i;
            return Genres[index].Name.ToString();
        }

        /// <summary>
        /// Calcs insertions of word from classifier list (root, seed or concept) to video descriptor.
        /// </summary>
        /// <param name="classifierWord">Word from root or seed or concept list.</param>
        /// <param name="videoInfo">Video descriptor</param>
        /// <returns></returns>
        double CalcInsertionsOfWordToDescriptor(string classifierWord, VideoDescriptor videoDescriptor, GenreResult genreResult, double weight)
        {
            double score = 0;

            foreach (var word in videoDescriptor.Title)
                if (word.Contains(classifierWord))
                { 
                    score += p1;
                    genreResult.TitleScore += p1 * weight;
                }
            foreach (var word in videoDescriptor.Tags)
                if (word.Contains(classifierWord))
                {
                    score += p2;
                    genreResult.TagsScore += p2 * weight;
                }
            foreach (var word in videoDescriptor.Description)
                if (word.Contains(classifierWord))
                {
                    score += p3;
                    genreResult.DescriptionScore += p3 * weight;
                }
            foreach (var word in videoDescriptor.Comments)
                if (word.Contains(classifierWord))
                {
                    score += p4;
                    genreResult.CommentsScore += p4 * weight;
                }
            if (UseSubs)
                foreach (var word in videoDescriptor.Subtitles)
                    if (word.Contains(classifierWord))
                    {
                        score += p5;
                        genreResult.SubtitilesScore += p5 * weight;
                    }

            return score * weight;
        }
    }
}
