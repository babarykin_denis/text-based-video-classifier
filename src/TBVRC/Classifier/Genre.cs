﻿using LemmaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classifier
{
    public class Genre
    {
        public Genre(string name)
        {
            Name = name;
            RootWords = new List<string>();
            SeedList = new List<string>();
            SynonymsList = new List<string>();
            SemanticList = new List<string>();
        }

        public string Name { get; set; }

        public List<string> RootWords { get; set; }

        public List<string> SeedList { get; set; }

        public List<string> SynonymsList { get; set; }

        public List<string> SemanticList { get; set; }

        //public void FillConceptList()
        //{
        //    var hash = new HashSet<string>();

        //    foreach (string word in RootWords)
        //        AddRangeToHash(hash, Lexicon.FindSynonyms(word, Wnlib.PartsOfSpeech.Noun, true) ?? new string[0]);
        //    foreach (string word in SeedList)
        //        AddRangeToHash(hash, Lexicon.FindSynonyms(word, Wnlib.PartsOfSpeech.Noun, true) ?? new string[0]);

        //    SynonymsList = hash.ToList();
        //}

        public void LemmatizeLists()
        {
            ILemmatizer lmtz = new LemmatizerPrebuiltCompact(LanguagePrebuilt.English);

            RootWords = RootWords.Select(w => lmtz.Lemmatize(w.ToLower())).ToList();
            SeedList = SeedList.Select(w => lmtz.Lemmatize(w.ToLower())).ToList();
            SynonymsList = SynonymsList.Select(w => lmtz.Lemmatize(w.ToLower())).ToList();
            SemanticList = SemanticList.Select(w => lmtz.Lemmatize(w.ToLower())).ToList();
        }
    }
}
