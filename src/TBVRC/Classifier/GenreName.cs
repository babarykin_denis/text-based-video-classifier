﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classifier
{
    public enum GenreName
    {
        Romance,
        Comedy,
        Horror,
        Sports,
        Tech
    }
}
