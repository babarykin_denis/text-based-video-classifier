﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classifier
{
    public class StopWordsLoader
    {
        static StopWordsLoader()
        {
            try
            {
                StopWords = File.ReadAllLines(Environment.CurrentDirectory + @"\" + ConfigurationManager.AppSettings["stopWordsFileName"]).ToList();
            }
            catch (Exception e)
            {
                throw new Exception("Error during StopWords list creating.", e);
            }
        }

        public static List<string> StopWords { get; set; }
    }
}
