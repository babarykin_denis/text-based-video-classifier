﻿using LemmaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTubeProcessor;

namespace Classifier
{
    public class VideoDescriptor
    {
        public VideoDescriptor(VideoInfo videoInfo)
        {
            ILemmatizer lmtz = new LemmatizerPrebuiltCompact(LanguagePrebuilt.English);

            VideoInfo vi = FilterSymbols(videoInfo);

            Title = vi.Title.Split(new char[] { ' ', '\n' }, StringSplitOptions.RemoveEmptyEntries).Select(w => lmtz.Lemmatize(w)).ToList();
            Tags = vi.Tags.SelectMany(tgs => tgs.Split(new char[] { ' ', '\n' }, StringSplitOptions.RemoveEmptyEntries)).Select(w => lmtz.Lemmatize(w)).ToList();
            Description = vi.Description.Split(new char[] { ' ', '\n' }, StringSplitOptions.RemoveEmptyEntries).Select(w => lmtz.Lemmatize(w)).ToList();
            Comments = vi.Comments.SelectMany(coms => coms.Split(new char[] { ' ', '\n' }, StringSplitOptions.RemoveEmptyEntries)).Select(w => lmtz.Lemmatize(w)).ToList();
            if (vi.Subtitles != null)
                Subtitles = vi.Subtitles.Split(new char[] { ' ', '\n' }, StringSplitOptions.RemoveEmptyEntries).Select(w => lmtz.Lemmatize(w)).ToList();
            else
                Subtitles = new List<string>();

            RemoveStopWords();
        }

        public List<string> Title { get; set; }

        public List<string> Tags { get; set; }

        public List<string> Description { get; set; }

        public List<string> Comments { get; set; }

        public List<string> Subtitles { get; set; }

        void RemoveStopWords()
        {
            foreach (var stopWord in StopWordsLoader.StopWords)
            {
                Title.RemoveAll(w => w == stopWord);
                Tags.RemoveAll(w => w == stopWord);
                Description.RemoveAll(w => w == stopWord);
                Comments.RemoveAll(w => w == stopWord);
                Subtitles.RemoveAll(w => w == stopWord);
            }
        }

        /// <summary>
        /// Filters all symbols except letters and lowers them.
        /// </summary>
        VideoInfo FilterSymbols(VideoInfo videoInfo)
        {
            VideoInfo filterdVideoInfo = new VideoInfo();

            filterdVideoInfo.Title = (videoInfo.Title != null) ? new string(videoInfo.Title.ToLower().Where(c => c == ' ' || c == '-' || (c >= 'a' && c <= 'z')).ToArray()) : "";
            filterdVideoInfo.Description = (videoInfo.Description != null) ? new string(videoInfo.Description.ToLower().Where(c => c == ' ' || c == '-' || (c >= 'a' && c <= 'z')).ToArray()) : "";
            filterdVideoInfo.Subtitles = (videoInfo.Subtitles != null) ? new string(videoInfo.Subtitles.ToLower().Where(c => c == ' ' || c == '-' || (c >= 'a' && c <= 'z')).ToArray()) : "";
            filterdVideoInfo.Tags = videoInfo.Tags.Select(w => new string(w.ToLower().Where(c => c == ' ' || c == '-' || (c >= 'a' && c <= 'z')).ToArray())).ToList();
            filterdVideoInfo.Comments = videoInfo.Comments.Select(w => new string(w.ToLower().Where(c => c == ' ' || c == '-' || (c >= 'a' && c <= 'z')).ToArray())).ToList();

            return filterdVideoInfo;
        }
    }
}
