﻿using LAIR.ResourceAPIs.WordNet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classifier
{
    public class GenresFactory
    {
        public GenresFactory()
        {
            try
            {
                Wordnet = new WordnetEngineWrapper(Environment.CurrentDirectory + @"\" + ConfigurationManager.AppSettings["wordnetBasesPath"], false);
            }
            catch(Exception e)
            {
                throw new Exception("Wordnet loading error.", e);
            }
        }

        WordnetEngineWrapper Wordnet { get; set; }

        public List<Genre> CreateGenres()
        {
            List<Genre> genres = new List<Genre>();
            try
            {
                genres = JsonConvert.DeserializeObject<List<Genre>>(File.ReadAllText(Environment.CurrentDirectory + @"\" + ConfigurationManager.AppSettings["genresFileName"]));
                foreach (var genre in genres)
                    FinalizeGenreBuilding(genre);

                bool normalizeSemanticLists = false;
                if (normalizeSemanticLists)
                {
                    int min = genres.Min(g => g.SemanticList.Count);
                    foreach (var genre in genres)
                        genre.SemanticList = genre.SemanticList.Take(min).ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error during Genres list creating.", e);
            }

            return genres;
        }

        void FinalizeGenreBuilding(Genre genre)
        {
            genre.LemmatizeLists();
            genre.SynonymsList = Wordnet.GetSynonymsList(genre.RootWords.Concat(genre.SeedList).ToList());
            genre.SemanticList = Wordnet.GetRelatedWordsList(genre.RootWords.Concat(genre.SeedList).Concat(genre.SynonymsList).ToList());
            //Wordnet.CreateSynonymsAndSemanticsList(genre.RootWords.Concat(genre.SeedList).ToList(), genre.SynonymsList, genre.SemanticList);
            genre.LemmatizeLists();

            // replacing all spaces
            genre.RootWords = genre.RootWords.Select(w => w.Replace(' ', '-')).ToList();
            genre.SeedList = genre.SeedList.Select(w => w.Replace(' ', '-')).ToList();
            genre.SynonymsList = genre.SynonymsList.Select(w => w.Replace(' ', '-').Replace('_', '-')).ToList();
            genre.SemanticList = genre.SemanticList.Select(w => w.Replace(' ', '-').Replace('_', '-')).ToList();
        }
    }
}
