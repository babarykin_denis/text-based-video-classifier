﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classifier
{
    public class GenreResult
    {
        public GenreResult()
        {
            TitleScore = TagsScore = DescriptionScore = CommentsScore = RootListScore = SeedListScore = SynonymsListScore = SubtitilesScore = 0.0;
        }

        #region DescriptorScore

        public double TitleScore { get; set; }

        public double TagsScore { get; set; }

        public double DescriptionScore { get; set; }

        public double CommentsScore { get; set; }

        public double SubtitilesScore { get; set; }

        #endregion

        #region ClassifierListsScore

        public double RootListScore { get; set; }

        public double SeedListScore { get; set; }

        public double SynonymsListScore { get; set; }

        public double SemanticListScore { get; set; }

        #endregion

        public double Sum
        {
            get { return TitleScore + TagsScore + DescriptionScore + CommentsScore + SubtitilesScore; }
        }

        public List<double> GetDescriptorScoresPercentage(double overallSum)
        {
            return new double[] { TitleScore, TagsScore, DescriptionScore, CommentsScore, SubtitilesScore }.Select(f => f / overallSum * 100).ToList();
        }

        public List<double> GetListsScoresPercentage(double overallSum)
        {
            return new double[] { RootListScore, SeedListScore, SynonymsListScore, SemanticListScore }.Select(f => f / overallSum * 100).ToList();
        }
    }
}
