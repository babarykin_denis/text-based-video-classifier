﻿using LAIR.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAIR.ResourceAPIs.WordNet
{
    public class WordnetEngineWrapper
    {
        /// <summary>Constructor</summary>
        /// <param name="wordNetDirectory">Path to WorNet directory (the one with the data and index files in it)</param>
        /// <param name="inMemory">Whether or not to store all data in memory. In-memory storage requires quite a bit of space
        /// but it is also very quick. The alternative (false) will cause the data to be searched on-disk with an efficient
        /// binary search algorithm.</param>
        public WordnetEngineWrapper(string wordNetDirectory, bool inMemory)
        {
            WordnetEngine = new WordNetEngine(wordNetDirectory, false);
        }

        WordNetEngine WordnetEngine { get; set; }

        /// <summary>
        /// Gets synonyms list for words in given list.
        /// </summary>
        public List<string> GetSynonymsList(List<string> initialList)
        {
            HashSet<string> overallHash = new HashSet<string>();

            foreach (string word in initialList)
                AddRangeToHash(overallHash, GetAllWordsFromSynsetsSet(WordnetEngine.GetSynSets(word)));

            return overallHash.ToList();
        }

        /// <summary>
        /// Gets list of related words for words in given list.
        /// </summary>
        public List<string> GetRelatedWordsList(List<string> initialList)
        {
            HashSet<string> overallHash = new HashSet<string>();

            foreach (string word in initialList)
            {
                HashSet<string> hash = new HashSet<string>();
                Set<SynSet> synSets = WordnetEngine.GetSynSets(word);

                foreach (var synset in synSets)
                    AddRangeToHash(hash, GetAllWordsFromSynsetsSet(GetRelatedSynsets(synset)));

                AddRangeToHash(overallHash, hash);
            }

            return overallHash.ToList();
        }

        /// <summary>
        /// Gets all synsets related to the given one.
        /// </summary>
        Set<SynSet> GetRelatedSynsets(SynSet synset)
        {
            WordNetEngine.SynSetRelation[] relationTypes = new WordNetEngine.SynSetRelation[] 
            {
                WordNetEngine.SynSetRelation.None,
                WordNetEngine.SynSetRelation.AlsoSee,
                //WordNetEngine.SynSetRelation.Antonym,
                WordNetEngine.SynSetRelation.Attribute,
                WordNetEngine.SynSetRelation.Cause,
                WordNetEngine.SynSetRelation.DerivationallyRelated,
                WordNetEngine.SynSetRelation.DerivedFromAdjective,
                WordNetEngine.SynSetRelation.Entailment,
                WordNetEngine.SynSetRelation.Hypernym,
                WordNetEngine.SynSetRelation.Hyponym,
                WordNetEngine.SynSetRelation.InstanceHypernym,
                WordNetEngine.SynSetRelation.InstanceHyponym,
                WordNetEngine.SynSetRelation.MemberHolonym,
                WordNetEngine.SynSetRelation.MemberMeronym,
                WordNetEngine.SynSetRelation.PartHolonym,
                WordNetEngine.SynSetRelation.ParticipleOfVerb,
                WordNetEngine.SynSetRelation.PartMeronym,
                WordNetEngine.SynSetRelation.Pertainym,
                WordNetEngine.SynSetRelation.RegionDomain,
                WordNetEngine.SynSetRelation.RegionDomainMember,
                WordNetEngine.SynSetRelation.SimilarTo,
                WordNetEngine.SynSetRelation.SubstanceHolonym,
                WordNetEngine.SynSetRelation.SubstanceMeronym,
                WordNetEngine.SynSetRelation.TopicDomain,
                WordNetEngine.SynSetRelation.TopicDomainMember,
                WordNetEngine.SynSetRelation.UsageDomain,
                WordNetEngine.SynSetRelation.UsageDomainMember,
                WordNetEngine.SynSetRelation.VerbGroup
            };
            HashSet<string> hash = new HashSet<string>();

            return synset.GetRelatedSynSets(relationTypes, false);
        }

        /// <summary>
        /// Gets HashSet of all words in given synsets
        /// </summary>
        HashSet<string> GetAllWordsFromSynsetsSet(Set<SynSet> synsets)
        {
            HashSet<string> hash = new HashSet<string>();

            foreach (var synset in synsets)
                AddRangeToHash(hash, synset.Words);

            return hash;
        }

        static void AddRangeToHash(HashSet<string> hash, IEnumerable<string> strings)
        {
            foreach (string str in strings)
                hash.Add(str);
        }
    }
}
